import java.util.Scanner;
import java.io.IOException;
import java.util.HashMap;

/**
 * Enumeration of colours.
 * 
 * Foreground:
 * Black, Red, Green, Orange, Blue, Magenta, Cyan, LightGray, Default.
 * Background:
 * Red, Green, Blue, Default.
 * 
 * by Lucky777.
 */
enum Col {
    BLACK,
    RED,
    GREEN,
    ORANGE,
    BLUE,
    MAGENTA,
    CYAN,
    LIGHTGRAY,
    DEFAULT,

    BG_RED,
    BG_GREEN,
    BG_BLUE,
    BG_DEFAULT;
}

public class LuckyTools {

    static Scanner kb = new Scanner(System.in);

    private HashMap<Col, Integer> codes;

    /**
     * Creates a LuckyTools object, with a HashTable of colours.
     */
    public LuckyTools() {
        codes = new HashMap<>();

        //================  FOREGROUND
        codes.put(Col.BLACK, 30);
        codes.put(Col.RED, 31);
        codes.put(Col.GREEN, 32);
        codes.put(Col.ORANGE, 33);
        codes.put(Col.BLUE, 34);
        codes.put(Col.MAGENTA, 35);
        codes.put(Col.CYAN, 36);
        codes.put(Col.LIGHTGRAY, 37);
        codes.put(Col.DEFAULT, 39);

        //================  BACKGROUND
        codes.put(Col.BG_RED, 41);
        codes.put(Col.BG_GREEN, 42);
        codes.put(Col.BG_BLUE, 44);
        codes.put(Col.BG_DEFAULT, 49);
    }

    /**
     * Returns the code to change a colour.
     * 
     * Code: "\033[" + ColorNumber + "m"
     * The ColourNumber is accessible in the HashTable of the object.
     * 
     * @param cn the wanted colour
     * @return "\033[" + cn + "m"
     */
    public String use(Col cn) {
        return "\033[" + this.codes.get(cn) + "m";
    }

    /**
     * Prints a String in a certain colour.
     * 
     * This method will print the String using the given colour, then change the colour back to default.
     * 
     * @param cn the wanted colour
     * @param txt the wanted String to print
     */
    public void print(Col cn, String txt) {
        System.out.print(this.use(cn));
        System.out.print(txt);
        System.out.print(this.use(Col.DEFAULT));
    }

    /**
     * Prints a String in a certain colour, ending with a \n.
     * 
     * This method will print the String using the given colour, then change the colour back to default.
     * This method will add a line break at the end of the text.
     * 
     * @param cn the wanted colour
     * @param txt the wanted String to print
     */
    public void println(Col cn, String txt) {
        System.out.print(this.use(cn));
        System.out.print(txt);
        System.out.println(this.use(Col.DEFAULT));
    }

    /**
     * Force the user to input a valid integer in a range of two integers.
     * 
     * @param msg The message we want to print
     * @param min The minimum value accepted
     * @param max The maximum value accepted
     * @return The valid integer
     */
    public static int askIntRange(String msg, int min, int max) {
        fastPrint("" + msg + " (" + min + "/" + max + ") : ");
        while (!kb.hasNextInt()) {
            fastPrint("! Please enter a valid number : ");
            kb.next();
        }
        int result = kb.nextInt();
        kb.nextLine(); /* Delete '\n' char in 'kb' buffer */
        while (result < min || result > max) {
            fastPrint("! Please enter a number between " + min + " and " + max + " : ");
            while (!kb.hasNextInt()) {
                fastPrint("! Please enter a valid number : ");
                kb.next();
            }
            result = kb.nextInt();
            kb.nextLine(); /* Delete '\n' char in 'kb' buffer */
        }
        return result;
    }

    /**
     * Choose a random integer in a range of two integers.
     * 
     * @param min The minimum value of the random integer
     * @param max The maximum value of the random integer
     * @return The random integer between min and max
     */
    public static int randomIntRange(int min, int max) {
        return (int) (min + Math.random() * (max - min + 1));
    }

    /**
     * Check if a variable is an integer or not.
     * 
     * @param item The variable we want to check
     * @return True or False if the variable is a valid integer or not
     */
    public static boolean isInt(String item) {
        try {
            Integer.parseInt(item);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * Print a message and wait for the user to press 'Enter'.
     */
    public static void enter() {
        fastPrint("Press enter to continue...");
        try {
            System.in.read();
        } catch (Exception ex) {
            fastPrint("! Unexpected 'read' error");
            sleep(1);
        }
    }

    /**
     * Lovely way to print a message like a typewriter The method waits 10 ms after.
     * printing each char of the msg.
     * 
     * @param msg The message we want to print
     */
    public static void fastPrint(String msg) {
        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) == '\n') {
                System.out.println();
            } else {
                System.out.print(msg.charAt(i));
                sleep(0.01);
            }
        }
    }

    /**
     * Lovely way to print a message like a typewriter The method waits 100 ms after.
     * printing each char of the msg Then waits 1 sec after the entire msg.
     * 
     * @param msg
     */
    public static void slowPrint(String msg) {
        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) == '\n') {
                System.out.println();
            } else {
                System.out.print(msg.charAt(i));
                sleep(0.1);
            }
        }
        sleep(1);
    }

    /**
     * The program waits a certain number of milliseconds before continuing.
     * 
     * @param s The number of milliseconds
     */
    public static void sleep(double s) {
        int ms = (int) (s * 1000);
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            fastPrint("! <error> ('sleep' method)");
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Clear the screen of the user.
     */
    public static void clearScreen() {
        String os = System.getProperty("os.name");
        try {
            if (os.contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033\143");
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("! <error> ('clearScreen' method)");
            sleep(1);
        }
    }
}